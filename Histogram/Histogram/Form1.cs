﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZedGraph;
using System.Windows.Forms;

namespace Histogram
{
    public partial class Form1 : Form
    {
        Bitmap img1, img2, img3;
        int curentImgWidth;
        int curentImgHeight;
        double zoom = 1;
        GraphPane myPane = new GraphPane();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnload_Click(object sender, EventArgs e)
        {
            myPane.CurveList.Clear();
            myPane = zedGraphControl1.GraphPane;
            myPane.XAxis.Title.Text = "Jumlah Pixel";
            myPane.YAxis.Title.Text = "NIlai Pixel";
            myPane.Title.Text = "Histogram";
            OpenFileDialog oFile = new OpenFileDialog();
            if (oFile.ShowDialog() == DialogResult.OK)
            {
                myPane.CurveList.Clear();
                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                img1 = new Bitmap(new Bitmap(oFile.FileName), pictureBox1.Width, pictureBox1.Height);
                pictureBox1.Image = img1;
                histo();
                myPane.AxisChange();
                zedGraphControl1.Refresh();
                curentImgWidth = Convert.ToInt32(pictureBox1.Width);
                curentImgHeight = Convert.ToInt32(pictureBox1.Width * img1.Height / img1.Width );
            }
        }

        private void btngrayscale_Click(object sender, EventArgs e)
        {
            int i, I;
            if (img1 != null)
            {
                img2 = new Bitmap(img1);
                for (i = 0; i <= img2.Width - 1; i++) {
                    for (I = 0; I <= img2.Height - 1; I++)
                    {
                        Color originalColor = img2.GetPixel(i,I);
                        int grayScale = (int)((originalColor.R * .3) + (originalColor.G * .59) + (originalColor.B * .11));
                        Color newColor = Color.FromArgb(grayScale,grayScale,grayScale);
                        img2.SetPixel(i,I,newColor);
                    }
                }
                pictureBox2.Image = img2;
            }
        }

        private void btnbinary_Click(object sender, EventArgs e)
        {
            int i, I, rata, nGreen, nRed, nBlue;
            if (img1 != null)
            {
                img3 = new Bitmap(img1);
                for (i = 0; i <= img3.Width - 1; i++)
                {
                    for (I = 0; I <= img3.Height - 1; I++)
                    {
                        Color pixelColor = img3.GetPixel(i,I);
                        nRed = pixelColor.R;
                        nGreen = pixelColor.G;
                        nBlue = pixelColor.B;
                        rata = Convert.ToInt32((nRed + nGreen + nBlue) / 3);
                        if (rata > 27)
                        {
                            rata = 255;
                        }
                        else {
                            rata = 0;
                        }
                        Color newpixelColor = Color.FromArgb(rata,rata,rata);
                        img3.SetPixel(i,I,newpixelColor);
                    }
                }
                pictureBox3.Image = img3;
            }
        }


        private void histo()
        { 
          int[,] temp = new int[256, 1];
          int nilaiPixel;
          PointPairList dataGraph = new PointPairList();
            for (int i = 0; i < img1.Width; i++)
            {
                for (int j = 0; j < img1.Height; j++)
                {
                    nilaiPixel = img1.GetPixel(i, j).R;
                    temp[nilaiPixel, 0] = temp[nilaiPixel, 0] + 1;
                    Convert.ToDouble(img1.GetPixel(i,j).R);
                }
             }
                for (int i = 0; i < 256; i++)
             {
                dataGraph.Add(i, temp[i, 0]);
             }
                LineItem myCurve = myPane.AddCurve("data", dataGraph, Color.Black,
                SymbolType.None);
                myCurve.Line.Fill = new Fill(Color.Black, Color.Black, 45f);
                myPane.XAxis.Scale.Max = 258;
                zedGraphControl1.AxisChange();
        }
    }
}
